const {
  printInvoice,
  printPreResume,
  printOrder,
  printTest,
} = require("./printService");

const PRINTER_IP = "192.168.1.49";

printTest(PRINTER_IP);

// printOrder(PRINTER_IP, {
//   target: "COCINA",
//   date: "12/10/2021",
//   time: "12:05:23 PM",
//   operator: "JOSE LUIS",
//   table: 2,
//   order: [
//     {
//       quantity: 1,
//       description: "Pollo en salsa de esparragos",
//       extra: "Parte pecho",
//     },
//     {
//       quantity: 2,
//       description: "Fetuchini a lo alfredo",
//     },
//     {
//       quantity: 1,
//       description: "Adobo con arroz",
//     },
//   ],
// });

// printOrder(PRINTER_IP, {
//   // isAnnulation: true,
//   target: "BARRA",
//   date: "12/10/2021",
//   time: "12:05:23 PM",
//   operator: "JOSE LUIS",
//   table: 2,
//   order: [
//     {
//       quantity: 1,
//       description: "PISCO SOUR",
//     },
//     {
//       quantity: 2,
//       description: "MARACUYA SOUR",
//     },
//     {
//       quantity: 1,
//       description: "MARGARITA",
//     },
//   ],
// });

// printPreResume(PRINTER_IP, {
//   date: "12/10/2021",
//   time: "12:05:23 PM",
//   operator: "JOSE LUIS",
//   table: 2,
//   subTotal: "93.22",
//   taxes: "16.78",
//   total: "110.00",
//   order: [
//     {
//       quantity: 1,
//       description: "Pollo en salsa de esparragos",
//       unitPrice: "45.00",
//       totalPrice: "45.00",
//     },
//     {
//       quantity: 2,
//       description: "CHAUFA DE POLLO",
//       unitPrice: "25.00",
//       totalPrice: "50.00",
//     },
//     {
//       quantity: 1,
//       description: "MARGARITA",
//       unitPrice: "15.00",
//       totalPrice: "15.00",
//     },
//   ],
// });

// printInvoice(PRINTER_IP, {
//   ruc: "20601343453",
//   phone: "+519999999",
//   authorizationNumber: "0000000000",
//   serialNumber: "001",
//   pers: 1,
//   pos: "Caja 01",
//   cashier: "caja01",
//   invoiceNumber: "0001-0018282",
//   date: "12/10/2021",
//   time: "12:05:23 PM",
//   operator: "JOSE LUIS",
//   table: 2,
//   discountPercent: "50.00%",
//   discount: "141.00",
//   subTotal: "93.22",
//   taxes: "16.78",
//   total: "110.00",
//   exchangeRate: "3.34",
//   order: [
//     {
//       quantity: 1,
//       description: "ARROZ CON PATO PERUANO",
//       unitPrice: "38.00",
//       totalPrice: "38.00",
//     },
//     {
//       quantity: 3,
//       description: "POLLO A LA PLANCHA",
//       unitPrice: "40.00",
//       totalPrice: "120.00",
//     },
//     {
//       quantity: 8,
//       description: "PISCO SOUR",
//       unitPrice: "15.00",
//       totalPrice: "120.00",
//     },
//     {
//       quantity: 2,
//       description: "TAPER",
//       unitPrice: "2.00",
//       totalPrice: "4.00",
//     },
//   ],
//   payments: [
//     {
//       description: "Tarjeta (T/.) VISA",
//       amount: "141.00",
//     },
//   ],
// });

const print = require("./print");
const { Template, padEnd, padStart } = require("./TemplateBuilder");

module.exports = (ip, data) => {
  const template = new Template();

  // title
  template.addLine("AVANCE DE CUENTA").setAlignCenter().setSizeLarge();
  template.addLine("COMPROBANTE NO AUTORIZADO").setAlignCenter().setSizeLarge();

  template.addSpace();

  // header
  const widthFirstColHeader = 30;
  // first line - date and time
  template.addLine(
    `${padEnd(`Fecha: ${data.date}`, widthFirstColHeader)} Hora: ${data.time}`
  );

  // second line - operator and table
  template.addLine(
    `${padEnd(`Mozo: ${data.operator}`, widthFirstColHeader)} Mesa: ${
      data.table
    }`
  );

  template.addSpace();

  // table products
  const WIDTH_QUANTITY = 6;
  const WIDTH_PRODUCT = 27;
  const WIDTH_UNIT_PRICE = 6;
  const WIDTH_TOTAL = 6;
  template.addLine(
    `${padEnd("Cant.", WIDTH_QUANTITY)} ${padEnd(
      "Producto.",
      WIDTH_PRODUCT
    )} ${padEnd("P.U.", WIDTH_UNIT_PRICE)} ${padStart("Total", WIDTH_TOTAL)}`
  );
  template.addDivisor();

  for (let i = 0; i < data.order.length; i += 1) {
    const order = data.order[i];
    template.addLine(
      `${padEnd(order.quantity, WIDTH_QUANTITY)} ${padEnd(
        order.description,
        WIDTH_PRODUCT
      )} ${padStart(order.unitPrice, WIDTH_UNIT_PRICE)} ${padStart(
        order.totalPrice,
        WIDTH_TOTAL
      )}`
    );
  }

  template.addDivisor();

  template.addSpace();

  template.addLine(
    `${padEnd("SUB TOTAL", 15)} ${padEnd("S/", 20)} ${padStart(
      data.subTotal,
      6
    )}`
  );
  template.addLine(
    `${padEnd("IGV", 15)} ${padEnd("S/", 20)} ${padStart(data.taxes, 6)}`
  );
  template.addLine(
    `${padEnd("TOTAL", 15)} ${padEnd("S/", 20)} ${padStart(data.total, 6)}`
  );

  template.addSpace();

  template.addLine(`RUC: __________________________________________`);
  template.addSpace();
  template.addLine(`RAZON SOCIAL: _________________________________`);
  template.addSpace();
  template.addLine(`_______________________________________________`);
  template.addSpace();
  template.addLine(`TELEFONO: _____________________________________`);
  template.addSpace();
  template.addLine(`DIRECCION: ____________________________________`);
  template.addSpace();
  template.addLine(`DISTRITO: _____________________________________`);
  template.addSpace();
  template.addLine(`PROVINCIA: ____________________________________`);

  console.log(template.draw());

  print(ip, template.toPrinter());
};

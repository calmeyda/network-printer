const printInvoice = require("./printInvoice");
const printOrder = require("./printOrder");
const printPreResume = require("./printPreResume");
const printTest = require("./printTest");

module.exports = {
  printInvoice,
  printOrder,
  printPreResume,
  printTest,
};

const escpos = require("escpos");
escpos.Network = require("escpos-network");

module.exports = async (ip, data) => {
  const device = new escpos.Network(ip);

  const options = { encoding: "GB18030" };

  const printer = new escpos.Printer(device, options);

  device.open((error) => {
    if (error) {
      return console.error(error);
    }

    //   console.log("ok");

    for (let i = 0; i < data.length; i += 1) {
      const row = data[i];

      switch (row.align) {
        case "C":
          printer.align("CT");
          break;
        case "R":
          printer.align("RT");
          break;
        default:
          printer.align("LT");
          break;
      }

      switch (+row.size) {
        case 1:
          printer.size(0.6, 0.8);
          break;
        case 2:
          printer.size(0.6, 0.2);
          break;
        default:
          printer.size(0.5, 0.5);
          break;
      }

      if (row.text) {
        if (row.text === "---") {
          printer.drawLine();
        } else {
          printer.text(row.text);
        }
      } else {
        printer.newLine();
      }
    }

    printer.cut();
    printer.close();
  });
};

const print = require("./print");
const { Template, padEnd, padStart } = require("./TemplateBuilder");

module.exports = (ip, data) => {
  const template = new Template();

  // title
  template.addLine("RESTAURANTE").setAlignCenter().setSizeLarge();
  template.addLine(`RUC: ${data.ruc} TELF: ${data.phone}`).setAlignCenter();
  template
    .addLine(`Nro. Autorización : ${data.authorizationNumber}`)
    .setAlignCenter();
  template.addLine(`Nro. SERIE : ${data.serialNumber}`).setAlignCenter();

  template.addSpace();

  template
    .addLine(`TICKET BOLTEA NRO : ${data.invoiceNumber}`)
    .setAlignCenter();
  template.addLine(`${data.date} ${data.time}`).setAlignCenter();
  template.addLine(
    `MESA: ${data.table}   MOZO: ${data.operator}   PERS: ${data.pers}`
  );
  template.addLine(`PUNTO DE VENTA: ${data.pos}`);
  template.addLine(`CAJERO: ${data.cashier}`);

  template.addSpace();

  // table products
  const WIDTH_QUANTITY = 6;
  const WIDTH_PRODUCT = 27;
  const WIDTH_UNIT_PRICE = 6;
  const WIDTH_TOTAL = 6;
  template.addLine(
    `${padEnd("Cant.", WIDTH_QUANTITY)} ${padEnd(
      "Producto.",
      WIDTH_PRODUCT
    )} ${padEnd("P.U.", WIDTH_UNIT_PRICE)} ${padStart("Total", WIDTH_TOTAL)}`
  );
  template.addDivisor();

  for (let i = 0; i < data.order.length; i += 1) {
    const order = data.order[i];
    template.addLine(
      `${padEnd(order.quantity, WIDTH_QUANTITY)} ${padEnd(
        order.description,
        WIDTH_PRODUCT
      )} ${padStart(order.unitPrice, WIDTH_UNIT_PRICE)} ${padStart(
        order.totalPrice,
        WIDTH_TOTAL
      )}`
    );
  }

  template.addDivisor();

  template.addSpace();

  template.addLine(
    `${padEnd("DESCUENTO", 15)} ${padEnd(
      `S/ (${data.discountPercent})`,
      20
    )} ${padStart(data.discount, 6)}`
  );
  template.addLine(
    `${padEnd("SUB TOTAL", 15)} ${padEnd("S/", 20)} ${padStart(
      data.subTotal,
      6
    )}`
  );
  template.addLine(
    `${padEnd("IGV", 15)} ${padEnd("S/ (18%)", 20)} ${padStart(data.taxes, 6)}`
  );
  template.addLine(
    `${padEnd("TOTAL", 15)} ${padEnd("S/", 20)} ${padStart(data.total, 6)}`
  );
  template.addLine(
    `${padEnd("PAGA CON", 15)} ${padEnd("S/", 20)} ${padStart(data.total, 6)}`
  );
  template.addLine(
    `${padEnd("VUETO", 15)} ${padEnd("S/", 20)} ${padStart(data.total, 6)}`
  );

  template.addSpace();

  template.addLine("FORMAS DE PAGO:");
  for (let i = 0; i < data.payments.length; i += 1) {
    const payment = data.payments[i];
    template.addLine(
      `${padEnd(`  ${payment.description}`, 40)} ${padStart(payment.amount, 6)}`
    );
  }

  template.addSpace();

  template.addLine(`TIPO DE CAMBIO: ${data.exchangeRate}`);

  console.log(template.draw());

  print(ip, template.toPrinter());
};

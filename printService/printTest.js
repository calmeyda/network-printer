const print = require("./print");
const { Template } = require("./TemplateBuilder");

module.exports = (ip, text = "Printer is working") => {
  const template = new Template();

  // title
  template.addLine(text).setAlignCenter().setSizeLarge();
  template.addSpace();
  template.addLine(text).setAlignCenter().setSizeMedium();
  template.addSpace();
  template.addLine(text).setAlignCenter();
  template.addSpace();

  console.log(template.draw());

  print(ip, template.toPrinter());
};

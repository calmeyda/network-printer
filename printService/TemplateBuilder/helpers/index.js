const padEnd = require("./padEnd");
const padStart = require("./padStart");

module.exports = {
  padEnd,
  padStart,
};

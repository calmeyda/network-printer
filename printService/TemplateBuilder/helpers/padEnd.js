module.exports = (text = " ", length) =>
  text.toString().substring(0, length).padEnd(length, " ");

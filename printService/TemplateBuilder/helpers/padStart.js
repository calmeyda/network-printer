module.exports = (text = " ", length) =>
  text.toString().substring(0, length).padStart(length, " ");

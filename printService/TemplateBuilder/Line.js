module.exports = class Line {
  constructor(text) {
    this.value = { align: "L", size: 3, style: "R", text };
  }

  // Align methods
  setAlign(align) {
    this.value.align = align;
    return this;
  }

  setAlignCenter() {
    this.setAlign("C");
    return this;
  }

  setAlignLeft() {
    this.setAlign("L");
    return this;
  }

  setAlignRight() {
    this.setAlign("R");
    return this;
  }

  // Size methods
  setSize(size) {
    this.value.size = size;
    return this;
  }

  setSizeLarge() {
    this.setSize(1);
    return this;
  }

  setSizeMedium() {
    this.setSize(2);
    return this;
  }

  setSizeRegular() {
    this.setSize(3);
    return this;
  }

  setSizeSmall() {
    this.setSize(4);
    return this;
  }

  getValue() {
    return this.value;
  }
};

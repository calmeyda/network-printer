const Line = require("./Line");

module.exports = class Template {
  constructor() {
    this.lines = [];
  }

  addLine(line) {
    const lineInstance = line instanceof Line ? line : new Line(line);

    this.lines.push(lineInstance);

    return lineInstance;
  }

  addDivisor() {
    return this.addLine("---");
  }

  addSpace() {
    return this.addLine("");
  }

  toPrinter() {
    return this.lines.map((line) => {
      return line.getValue();
    });
  }

  draw() {
    return this.lines.map((line) => {
      const { align, size, style, text } = line.getValue();
      return `${align}${size}${style}*${text}`;
    });
  }
};

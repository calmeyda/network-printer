const print = require("./print");
const { Template, padEnd } = require("./TemplateBuilder");

module.exports = (ip, data) => {
  const { isAnnulation } = data;

  const template = new Template();

  // title
  template
    .addLine(isAnnulation ? "ANULACION" : "NUEVO PEDIDO")
    .setAlignCenter()
    .setSizeLarge();
  template.addLine(data.target).setAlignCenter().setSizeMedium();

  template.addSpace();

  // header
  const widthFirstColHeader = 30;
  // first line - date and time
  template.addLine(
    `${padEnd(`Fecha: ${data.date}`, widthFirstColHeader)} Hora: ${data.time}`
  );

  // second line - operator and table
  template.addLine(
    `${padEnd(`Mozo: ${data.operator}`, widthFirstColHeader)} Mesa: ${
      data.table
    }`
  );

  template.addSpace();

  // table products
  template.addLine("Cant. Producto");
  template.addDivisor();

  for (let i = 0; i < data.order.length; i += 1) {
    const order = data.order[i];
    template.addLine(`${padEnd(order.quantity, 6)}${order.description}`);
    if (order.extra) {
      template.addLine(`${padEnd("", 6)}${order.extra}`);
    }
  }

  template.addDivisor();
  template.addSpace();

  console.log(template.draw());

  print(ip, template.toPrinter());
};
